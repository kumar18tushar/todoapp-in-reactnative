import React, {useState} from 'react';
import { StyleSheet, Text, View, Button, TextInput, ScrollView, FlatList,Alert} from 'react-native';
import TodoItems from './components/Todoitems';
import InputAndButton from './components/Inputandbutton';
import { AntDesign, Foundation } from '@expo/vector-icons';
import * as Font from 'expo-font';
import {AppLoading} from 'expo';

const getFonts = () => {
  return Font.loadAsync({
    'nunito-regular':require('./assets/Fonts/Nunito-Regular.ttf'),
    'nunito-bold':require('./assets/Fonts/Nunito-Bold.ttf'),
    'nunito-semibold':require('./assets/Fonts/Nunito-SemiBold.ttf'),
    'nunito-semibolditalic':require('./assets/Fonts/Nunito-SemiBoldItalic.ttf'),
    'bowlbyone':require('./assets/Fonts/BowlbyOneSC-Regular.ttf'),
    'lobster':require('./assets/Fonts/Lobster-Regular.ttf')
  })
}

export default function App() {  
  const [Fontloaded, setFontloaded] = useState(false);
  const[Todos, setTodos] = useState([]);
  const min = 1;
  const max = 1000000;

  const ButtonPress = (TodoVal) => {
    const currTodo = TodoVal;

    if(currTodo.length>=3) { 
      if(TodoVal === 'clear_all'){
        setTodos([]);                   // for reseting purpose
        return;
      }
      setTodos(Todos => [{key:(Math.floor(Math.random() * (+max - +min)) + +min).toString(), value:currTodo}, ...Todos]);
    }
    else {
      Alert.alert("OOPS !", "A todo must be atleast three chars long", [
        {
          text:'UNDERSTOOD'
        }
      ])
    }
  };


  const removeTodo = (Item) =>{
    const currTodos = Todos.filter(todo=> todo.key!=Item.key);
    setTodos(currTodos);
    console.log(Todos);
  }


  if(Fontloaded){
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.headerText}>Todos  </Text>
        <Foundation name="clipboard-pencil" size={70} color="white"/> 
      </View>

        <View style={styles.screen}>
        <InputAndButton onaddTodo = {ButtonPress}/>
        
        <View style={styles.list}>
        <FlatList  
          data ={Todos} 
          renderItem = {itemData => (
            <TodoItems onTouch={()=>{removeTodo(itemData.item)}} todo = {itemData.item.value}/>
        )}/></View>      
        </View>
    </View>   
  );}
  else { 
    return(
      <AppLoading
        startAsync={getFonts}
        onFinish={()=>setFontloaded(true)}
      />
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1
  }
  ,screen:{
    padding:50,
    //backgroundColor:'#ddd',
    flex:1
    //paddingBottom:100
  },

  header:{
    // flex:1,
    paddingTop:40,
    paddingLeft:20,
    paddingBottom:25,
    //paddingLeft:70,
    backgroundColor:'#03A9F4',
    flexDirection:'row',
    alignItems:'center'
    
  },

  list:{
    flex:1,
  },

  headerText:{
    color:'#F5F5F5',
    fontSize:55,
    fontFamily:'bowlbyone',
    //fontWeight:'bold',
  }

});

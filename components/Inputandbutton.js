import React, {useState} from 'react';
import { StyleSheet, Text, View, Button, TextInput} from 'react-native';

const InputAndButton = (props)=>{

    const [Todo, setTodo] = useState('');

    const setTodofromInput = (inputgiven) => {
        setTodo(inputgiven);
      };


    return(
        <View style={styles.inputAndButton}>
        <TextInput 
        placeholder="Type in a Todo...." 
        style={styles.inputBox} 
        onChangeText={setTodofromInput}
        value={Todo}
        />
        <Button title="Add" onPress={() => {props.onaddTodo(Todo);{if(Todo.length>=3) setTodo('');}}}/>
      </View>
    )
}

const styles = StyleSheet.create({

    inputAndButton:{
        //flex:1,
        flexDirection:'row',
        justifyContent:'space-around',
        alignItems:'center',
        paddingBottom:20
    },

    inputBox:{
        //flex:1,
        width:'82%',
        borderBottomColor:'#000000',
        borderBottomWidth:3, 
        padding:5,
        // shadowColor:'black',
        // shadowOffset:{width:0,height:100},
        // shadowRadius:0,
        // shadowOpacity:0.26,
        // elevation:6
    },
});

export default InputAndButton;
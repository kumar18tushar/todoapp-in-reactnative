import React from 'react'
import { Text, View, StyleSheet, TouchableOpacity, Button} from 'react-native';
import { AntDesign } from '@expo/vector-icons';

const TodoItems = (props)=> {
    return (
      
        <View style = {styles.style_TodoItem}>

          <View style={styles.TodoText}>
          <Text style={styles.Todopart}>{props.todo}</Text>
          </View>

          <TouchableOpacity onPress={props.onTouch} >
          <View style={styles.Icon}>
          <AntDesign name="delete" size={32} color="black"/> 
          </View>
          </TouchableOpacity>
        </View>
    )
};

const styles = StyleSheet.create({

style_TodoItem:{
    paddingBottom:10,
    paddingLeft:20,
    paddingTop:10,
    paddingRight:10,
    backgroundColor:'#CFD8DC',
    borderColor:'black',
    borderWidth:1,

    borderRadius:10,
    marginVertical:5,
    flexDirection:'row',
    alignItems:'center',
  },

  Todopart:{
    fontSize:17,
    fontFamily:'nunito-semibolditalic'
  },

TodoText:{
  //paddingRight:8,
  flex:3,
  marginRight:60
},

Icon:{
  flex:1

}

});



export default TodoItems;